import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      <Navigation />
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
