import "./Navigation.css";

function Navigation() {
  // TODO get user from props drilling
  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/* TODO check if user is connected */}
      <li>Username here / Please log in</li>
    </ul>
  );
}

export default Navigation;
